FROM python:3.10
COPY ./flaskapp/app.py /usr/srv/app/
WORKDIR /usr/srv/app/
COPY ./flaskapp/requirements.txt /usr/src/app/requirements.txt
RUN python3 -m pip install -r /usr/src/app/requirements.txt
ENTRYPOINT ["python"]
CMD ["app.py"]
