# Projet ydays

## Objectif du Projet

Déployer une image Docker dans un cluster Kubernetes à l'aide de GitLab CI et ArgoCD.

## Environnement de travail

MinKube sera déployé localement sur ma machine qui est installée sous le système d’exploitation Xubuntu 20.04 LTS

## Configuration des depôts Docker

```
sudo apt update
sudo apt install ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
## Installation de Docker Engine

```
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io
sudo systemctl enable docker.service
sudo systemctl enable containerd.service
```
## Linux post``` Installation

```
sudo usermod -aG docker $USER
newgrp docker
```
## Installation de Minikube

```
sudo apt install conntrack -y
wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
mv minikube-linux-amd64 /usr/local/bin/minikube
chmod +x /usr/local/bin/minikube
```
## Installation de Kubectl

```
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt update -y && sudo apt install kubectl kubeadm kubelet -y
```

## Démarrer Minikube et vérification post Installation

```
minikube start
minikube status
kubectl cluster-info
```
## Installation Argocd

```
minikube addons enable ingress
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/master/manifests/install.yaml
kubectl port-forward svc/argocd-server -n argocd 8080:443
```
Commande pour récupérer le mot de passe du compte admin

```
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d ; echo
```
## Intégration de l'application dans Argocd

Vous pouvez vous appuyer sur le code suivant pour créer votre nouvelle application dans Argocd

```
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: flaskapp
spec:
  destination:
    namespace: default
    server: 'https://kubernetes.default.svc'
  source:
    path: manifest
    repoURL: 'https://gitlab.com/norelissme/gitops.git'
    targetRevision: HEAD
  project: default
  syncPolicy:
    automated:
      prune: true
```

## Accéder à l'application

Vous pouvez récupérer l'URL de l'application à l'aide de la commande suivante :

```
minikube service flaskapp-service --url
```
